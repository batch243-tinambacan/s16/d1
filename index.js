// console.log("hi")

// Arithmetic Operators

let x=1397;
console.log("X is: " + x);
let y=7831;
console.log("Y is: " + y);

// Addition
let sum = x+y;
console.log("Result of addition operator: " + sum);

// Difference operator
let difference = x-y;
console.log("Result of Difference operator: " + difference);

// Multiplication operator
let multiplication = x*y;
console.log("Result of multiplication operator: " + multiplication);

// Division operator
let quotient = x/y;
console.log("Result of division operator: " + quotient);

// Modulo operator
let remainder = y%x;
console.log("Result of modulo: " + remainder);
let secondRemainder = x%y;
console.log("Result of modulo: " + secondRemainder);


// Assignment Operator

	// Basic Assignment (=)
	let assignmentNumber = 8;
	console.log(assignmentNumber);

	// Addition Assignment (+=)
	// assignmentNumber = assignmentNumber + 2
	// console.log(assignmentNumber);

	assignmentNumber += 2;
	console.log("Result of Addition Assignment Operator: " + assignmentNumber);	

	// Subtract assignment operator
	assignmentNumber -= 2;
	console.log("Result of Subtraction Assignment Operator: " + assignmentNumber);	

	// Multiplication assignment operator
	assignmentNumber *= 4;
	console.log("Result of multiplication Assignment Operator: " + assignmentNumber);

	// Division assignment operator
	assignmentNumber /= 8;
	console.log("Result of division Assignment Operator: " + assignmentNumber);


// Multiple Operators and Parentheses
	let mdas = 1+2-3*4/5;
	console.log("Result of mdas rule: " + mdas)

	let pemdas=1+(2-3)*(4/5);
	console.log("Result of pemdas rule: " + pemdas)


// Increment & Decrement
let z=1;

	// Pre-increment
	let increment= ++z
	console.log( "Result of z in pre-increment: " + z);
	console.log( "Result of increment in pre-increment: " + increment);


	// Post-increment
	increment= z++;
	console.log( "Result of z in post-increment: " + z);
	console.log( "Result of increment in post-increment: " + increment);
	increment = z++
	console.log("Result of increment in post-increment: " + increment);

	let p=0;

	// pre-decrement

	let decrement = --p;
	console.log("Result of z in pre-decrement: " + p);
	console.log("Result of decrement in pre-decrement: " + decrement);

	// post-decrement
	decrement=p--;
	console.log("Result of z in post-decrement: " + p);
	console.log("Result of decrement in post-decrement: " + decrement);

// Type Coercion

	let numA = '10';
	let numB = 12;

	let coercion = numA + numB;
	console.log(coercion);

	let numC = 16;
	let numD = 14;

	let numE = true + 1;
	console.log(numE);

// Comparison Operators
	let juan = "juan";

	// Equality operator (==)

	console.log(1==1);
	console.log(1=='1')

	// Strict equality operator
	console.log(1==='1');
	console.log('juan' == juan);

	// Inequality operator (!=)
	console.log(1 !=1);
	console.log(1 !=2);
	console.log(1 != '1');

	// Strict Operator
	console.log(1 !== '1')


// Relational Operators
	let a = 50;
	let b = 65;

	// GT Greater than operator (>)
	let isGreaterThan = a > b;
	console.log(isGreaterThan)

	// LT less than operator (<)
	let isLessThan = a < b;
	console.log(isLessThan);

	// GTE Greater than or equal operator (>=)
	let isGTorEqual = a >= b;
	console.log(isGTorEqual);

	// LTE Less than or equal operator (<=)
	let isLTorEqual = a <= b;
	console.log(isLTorEqual);

	let numStr = "30";

	console.log(a>numStr);

	console.log(b <= numStr);

// Logical Operator

		let isLegalAge = true;
		let isRegistered = false;

		// Logical AND Operator (&& - double ampersand)
		let allRequirementsMet = isLegalAge && isRegistered;
		console.log(allRequirementsMet);

		// Logical OR Operator (|| - double pipe)
		let someRequirementsMet = isLegalAge || isRegistered;
		console.log(someRequirementsMet);

		// Logical NOT Operator (! - Exclamation point)
		let someRequirementsNotMet = !isRegistered;
		console.log(someRequirementsMet);